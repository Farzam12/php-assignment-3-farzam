<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::POST('login', 'User\UserController@login');
Route::POST('logout', 'User\UserController@logout');
Route::POST('logoutfromalldevices', 'User\UserController@logoutfromalldevices');
Route::POST('logoutallusers', 'User\UserController@logoutallusers');
Route::POST('refresh', 'User\UserController@refresh');
Route::GET('me', 'User\UserController@userProfile');
Route::POST('register','User\UserController@register');
Route::POST('forgot-password', 'User\ForgetPasswordController@forgotPassword');
Route::POST('password-recreate', 'User\ForgetPasswordController@reset')->name('password-recreate');
Route::GET('reset-password', 'User\ForgetPasswordController@passwordreset')->name('password.reset');
Route::GET('test', 'User\UserController@test');
Route::POST('store','S3\S3Controller@store');
Route::POST('update','User\UserController@userUpdate');

