<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::GET('index','Inventory\ProductController@index');
Route::POST('create','Inventory\ProductController@create');
Route::POST('update','Inventory\ProductController@update');
Route::DELETE('delete','Inventory\ProductController@delete');


Route::GET('C_index','Inventory\CategoryController@index');
Route::POST('C_create','Inventory\CategoryController@create');
Route::POST('C_update','Inventory\CategoryController@update');
Route::DELETE('C_delete','Inventory\CategoryController@delete');


Route::GET('O_index','Inventory\OptionController@index');
Route::POST('O_create','Inventory\OptionController@create');
Route::POST('O_update','Inventory\OptionController@update');
Route::DELETE('O_delete','Inventory\OptionController@delete');

Route::GET('T_index','Inventory\TypeController@index');
Route::POST('T_create','Inventory\TypeController@create');
Route::POST('T_update','Inventory\TypeController@update');
Route::DELETE('T_delete','Inventory\TypeController@delete');

Route::GET('I_index','Inventory\InventoryController@index');
Route::DELETE('P_delete','Inventory\ProductController@deletePhotos');

