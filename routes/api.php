<?php

use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', 'User\UserController@AuthRouteAPI');
Route::GET('test', 'User\UserController@test');
