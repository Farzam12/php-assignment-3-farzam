<?php

namespace App\Console\Commands;

use App\Mail\ProductsMail;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class WordOfTheDay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'word:day';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a Daily email to all users with a word and its meaning';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $words =  'Hurry Up!!! New Products are waiting for you';

        $users = User::all();
        foreach ($users as $user) {
            Mail::to($user['email'])->send(new ProductsMail());
//            Mail::raw($words, function ($mail) use ($user) {
//                $mail->from('farzam.a@ovada.com');
//                $mail->to($user->email)
//                    ->subject('New Arrivals!!!');
//            });
        }
        $this->info('Mail sent to All Users');
    }
}
