<?php

namespace App\Listeners;

use App\Events\RegisterMail;
use App\Mail\MyTestMail;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailToUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  RegisterMail  $event
     * @return void
     */
    public function handle(RegisterMail $event)
    {
        $user = User::find($event->user)->first()->toArray();
        $details = [
            'name' => $user['name'],
            'subject' => 'You are registered'
        ];

        Mail::to($user['email'])->send(new MyTestMail($details));

    }

}
