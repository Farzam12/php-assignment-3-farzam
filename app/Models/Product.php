<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name', 'cost_price', 'sale_price','description','category_id','type_id','option_id','created_by','updated_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       'created_by','updated_by','created_at','updated_at','deleted_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function category(){
        return $this->belongsTo(Category::class,'category_id');
    }
    public function option(){
        return $this->belongsTo(Option::class,'option_id');
    }
    public function type(){
        return $this->belongsTo(Type::class,'type_id');
    }
    public function image()
    {
        return $this->morphOne(Images::class, 'model');
    }
}
