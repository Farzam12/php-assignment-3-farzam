<?php

namespace App\Http\Controllers\User;

use App\Events\RegisterMail;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\Add;
use App\Http\Requests\User\Login;
use App\Http\Requests\User\Update;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{
    //jwt middleware
    function __construct()
    {
        $this->middleware('jwt.verify');
    }


    public function index()
    {
        return view('welcome');
    }


    public function register(Add $request)
    {
        //Registering a user in database
        $user = User::create(array_merge(
            [
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'address' => $request->address
            ]
        ));

        //Will give browser info where api was hit
        $browser_info = $request->userAgent();

        $token = JWTAuth::fromUser($user);
        //User will be logged in as it'll be registered
        DB::table('tokens')
            ->insert([
                'user_id' => $user->id,
                'created_by' => $user->id,
                'token' => $token,
                'browser_info' => $browser_info
            ]);
        //Registeration mail will be sent through this event.
        event(new RegisterMail($user));

        //Take image as user's dp
        $file = $request->file('image');
        $file_name = time() . $file->getClientOriginalName();
        $filePath = 'farzam-images/' . $file_name;
        $s3 = Storage::disk('s3');
        $status = $s3->put($filePath, file_get_contents($file), 'public');
        if ($status) {

            $imageurl = $s3->url($filePath);
            $user->image()->updateOrCreate(
                [
                    'model_id' => $user['id'],
                    'model_type' => 'App\Models\User'
                ],
                [
                    'file_name' => $filePath,
                ]);
            $user['image_url'] = $imageurl;
            $user['token']=$token;

            return $this->response(true, "User is Registered", $user);
        }
    }

    public function login(Login $request)
    {
        $browser_info = $request->header('user-agent');
     //Will get the user having such email.
        $user = User::where('email', $request['email'])->first();
     //Check whether user is already logged in or not
        $browser = DB::table('tokens')->where('user_id', $user->id)->where('browser_info', $browser_info)->where('status',1)->get();
     //check for updation or creation of image
        $image = DB::table('images')->where('model_id', $user->id)->first();
        $imageUrl = $image->file_name;

        if ($browser->first() == null) {
            $credentials = $request->only('email', 'password');
            try {
                if (!$token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 400);
                }
            } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
            }
            //If user is not already logged in
            DB::table('tokens')
                ->insert([
                    'user_id' => $user->id,
                    'created_by' => $user->id,
                    'token' => $token,
                    'browser_info' => $browser_info
                ]);
            $user['token']=$token;
            $user['profile_pic']=$imageUrl;
            return $this->response(true,"User is logged in",$user);
        }
          else
          {
              $user['token']=$browser[0]->token;
              return $this->response(true,"User is already logged in",$user);
        }
    }


    public function logout()
    {
        //Getting token of logged in user
        $token=JWTAuth::parseToken()->getToken();
        try {
            JWTAuth::parseToken()->authenticate();
            JWTAuth::invalidate($token);
            //Change status to 0 to show him logged out
            DB::table('tokens')->where('token', $token)->update(['status' => 0]);
        }
        catch (JWTException $e) {
            return $this->response(400,"Error",null);
        }
        return $this->response(true,"User is logged out",null);
    }


    public function logoutfromalldevices()
    {
        $token=JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        //Getting tokens of a user logged in from different devices
        $tokens = DB::table('tokens')->select('token')->where('user_id', $user['id'])->where('status',1)->get();
        foreach ($tokens as $token)
        {
            //Invalidating them and changing status to 0 for each token
            JWTAuth::invalidate($token->token);
            DB::table('tokens')->where('token', $token->token)->update(['status' => 0]);
        }
        return $this->response(true, 'Successfully logged out of all devices',null);
    }



    public function userProfile()
    {
        $token = JWTAuth::parseToken()->authenticate();
        return $this->response(true, 'User',$token);
    }



    public function logoutallusers()
    {
        //Will get tokens of all users with status 1
        $tokens = DB::table('tokens')->where('status',1)->get();
        foreach ($tokens as $token)
        {
            //Invalidating them and changing status to 0 for each token
            JWTAuth::invalidate($token->token);
            DB::table('tokens')->where('token', $token->token)->update(['status' => 0]);
        }
        return $this->response(true, 'Successfully logged out all users',null);
    }


    public function userUpdate(Update $request)
    {
        //Getting logged in user
        $token=JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        //updating required info
        User::where('id',$user->id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'address' => $request->address,
            'updated_by' => $user->id
        ]);

        $file = $request->file('image');
        $file_name = time() . $file->getClientOriginalName();
        $filePath = 'farzam-images/' . $file_name;
        $s3 = Storage::disk('s3');
        $status = $s3->put($filePath, file_get_contents($file), 'public');


        if ($status) {
            $imageurl = $s3->url($filePath);
            $user->image()->updateOrCreate(
                [
                    'model_id' => $user['id'],
                    'model_type' => 'App\Models\User'
                ],
                [
                    'file_name' => $imageurl,
                ]);

            $token = JWTAuth::fromUser($user);
            $user['image_url'] = $imageurl;
            $user['token'] = $token;

            return $this->response(true, "User is Updated", $user);

        }
    }



    public function response($status,$message,$data)
    {
        return response()->json(['status'=>$status,'message'=>$message,'data'=>$data]);
    }




    protected function createNewToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }

}
