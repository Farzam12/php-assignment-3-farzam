<?php

namespace App\Http\Controllers\S3;

use App\Http\Controllers\Controller;
use App\Models\Images;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Facades\JWTAuth;

class S3Controller extends Controller
{
   public static function index (Request $request)
   {
       $filename = $request->get("filename");
       return Storage::disk('s3')->get($filename);
   }


    public function store(Request $request)
    {
        $token = JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        $file = $request->file('image');
        $file_name = time().$file->getClientOriginalName();
        $filePath = 'farzam_images/' . $file_name;
        // Storage::disk('s3')->put($filePath, file_get_contents($file));
        $s3 = Storage::disk('s3');
        $status = $s3->put($filePath, file_get_contents($file),'public');

        if ($status) {
            $imageurl = $s3->url($filePath);
            $user->image()->updateOrCreate(
                [
                    'model_id' => $user->id,
                    'model_type' => 'App\Models\User'
                ],
                [
                    'file_name' => $imageurl,
                ]);$user['image_url'] = $imageurl;

            return $this->response(true, "Picture Uploaded successfully", $user);
        } else {
            return $this->response(true, "Pic is not Uploaded", null);
        }
    }
    public static function deleteProductphotos($ids)
    {
        foreach ($ids as $id) {
            if(Images::where('id',$id['id'])->exists()) {
                $product = Images::find($id['id']);
                $image_Name = $product['file_name'];
                $status = Storage::disk('s3')->delete($image_Name);
                if ($status) {
                    $product->delete();
                }
            }
        }

    }

    public function response($status,$message,$data)
    {
        return response()->json(['status'=>$status,'message'=>$message,'data'=>$data]);
    }
}
