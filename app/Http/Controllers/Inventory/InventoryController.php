<?php

namespace App\Http\Controllers\Inventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InventoryController extends Controller
{
    public function index()
    {
        //Will show whole inventory through view.
        $inventory=DB::table('product_view')->get();
        return $this->response(true, "Whole inventory", $inventory);
    }
    public function response($status,$message,$data)
    {
        return response()->json(['status'=>$status,'message'=>$message,'data'=>$data]);
    }
}
