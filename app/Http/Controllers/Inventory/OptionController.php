<?php

namespace App\Http\Controllers\Inventory;

use App\Http\Controllers\Controller;


use App\Http\Requests\Inventory\Option\Add;
use App\Http\Requests\Inventory\Option\Update;
use App\Http\Requests\Inventory\Option\Delete;
use App\Models\Option;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class OptionController extends Controller
{
    public function index()
    {
        //Will show all options.
        $option=Option::all();
        return $this->response(true,"All Products",$option);
    }
    public function create(Add $request)
    {
        //Get logged in user
        $token=JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        //Add new type
        $option = Option::create([
            'size' => $request['size'],
            'color' => $request['color'],
            'created_by'=>$user->id
        ]);
        return $this->response(true,"Option is Added",$option);
    }

    public function update(Update $request)
    {
        //Get logged in user
        $token = JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        //Get id of option to update
        $option_id = Option::find($request['id']);
        //Check whether option exists ot not
        if ($option_id == null) {
            return $this->response(true, "Option is not available", null);
        } else {
            $option = $request->all();
            $request['updated_by'] = $user->id;
            Option::where('id', $option_id->id)->update($request->all());
            return $this->response(true, "Option is Updated", $option);
        }
    }
    public function delete(Delete $request)
    {
        //Get id of option to delete
        $id= Option::find($request['id']);
        //Check whether option exists ot not
        if($id == null){
            return $this->response(true,"Option is not available",null);
        }
        else {
            $option = Option::find($id->id);
            $option->product()->update(['option_id'=>null]);
            $option->delete();
            return $this->response(true, "Option is deleted", null);
        }
    }







    public function response($status,$message,$data){
        return response()->json(['status'=>$status,'message'=>$message,'data'=>$data]);
    }
}
