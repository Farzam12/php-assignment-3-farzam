<?php

namespace App\Http\Controllers\Inventory;

use App\Http\Controllers\Controller;
use App\Http\Requests\Inventory\Type\Add;
use App\Http\Requests\Inventory\Type\Delete;
use App\Http\Requests\Inventory\Type\Update;
use App\Models\Type;
use Tymon\JWTAuth\Facades\JWTAuth;

class TypeController extends Controller
{
    public function index()
    {
        //Will show all types
        $type=Type::all();
        return $this->response(true,"All Types",$type);
    }
    public function create(Add $request)
    {
        //Get logged in user
        $token=JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        //Add new type
        $type = Type::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'created_by'=>$user->id
        ]);
        return $this->response(true,"Type is Added",$type);
    }

    public function update(Update $request)
    {
        //Get logged in user
        $token = JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        //Get id of type to update
        $type_id = Type::find($request['id']);
        //Check whether type exists ot not
        if ($type_id == null) {
            return $this->response(true, "Type is not available", null);
        }
        else{
            $type = $request->all();
            $request['updated_by'] = $user->id;
            Type::where('id', $type_id->id)->update($request->all());
            return $this->response(true, "Type is Updated", $type);
        }
    }
    public function delete(Delete $request)
    {
        //Get id of type to delete
        $id= Type::find($request['id']);
        //Check whether type exists ot not
        if($id == null) {
            return $this->response(true, "Type is not available", null);
        }
        else{
            $type = Type::find($id->id);
            $type->product()->update(['type_id'=>null]);
            $type->delete();
            return $this->response(true, "Type is deleted", null);

        }
    }

    public function response($status,$message,$data)
    {
        return response()->json(['status'=>$status,'message'=>$message,'data'=>$data]);
    }
}
