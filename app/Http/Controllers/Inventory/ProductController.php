<?php

namespace App\Http\Controllers\Inventory;

use App\Http\Controllers\Controller;
use App\Http\Controllers\S3\S3Controller;
use App\Http\Requests\Inventory\Product\Add;
use App\Http\Requests\Inventory\Product\Update;
use App\Http\Requests\Inventory\Product\Delete;
use App\Models\Images;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProductController extends Controller
{
    public function index()
    {
        //Will show all products
        $product=Product::all();
        return $this->response(true,"All Products",$product);
    }
    public function create(Add $request)
    {
        //Get logged in user
        $token=JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        //Add new Product
        $product = Product::create([
            'name' => $request['name'],
            'cost_price' => $request['cost_price'],
            'sale_price' => $request['sale_price'],
            'description' => $request['description'],
            'category_id'=> $request['category_id'],
            'type_id'=> $request['type_id'],
            'option_id'=> $request['option_id'],
            'created_by'=>$user->id
        ]);
        //Take images of products in files
        $files = $request->file('image');
        $i=1;
        //Loop to upload images related to that product.
        foreach($files as $file){
        $file_name = time().$file->getClientOriginalName();
        $filePath = 'farzam_images/' . $file_name;
        $s3 = Storage::disk('s3');
        $status = $s3->put($filePath, file_get_contents($file),'public');
        if ($status) {
            $imageurl = $s3->url($filePath);
            $product->image()->insert(
                [
                    'model_id' => $product->id,
                    'model_type' => 'App\Models\Product',
                    'file_name' => $filePath
                ]);
            $product['image_url'.$i] = $imageurl;
            $i++;

        } else {
            return $this->response(true, "Pic is not Uploaded", null);
        }

    }
        return $this->response(true, "Picture Uploaded successfully", $product);
    }

    public function update(Update $request)
    {
        //Get logged in user
        $token = JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        //Get product to update
        $product= Product::find($request['id']);
        //Update required info
        Product::where('id', $product->id)->update([
            'name' => $request['name'],
            'cost_price' => $request['cost_price'],
            'sale_price' => $request['sale_price'],
            'description' => $request['description'],
            'category_id'=> $request['category_id'],
            'type_id'=> $request['type_id'],
            'option_id'=> $request['option_id'],
            'updated_by'=>$user->id
        ]);
        //Take new images of products in files
        $files = $request->file('image');
        $i = 1;
        //Loop to upload images related to that product.
        foreach ($files as $file) {
            $file_name = time() . $file->getClientOriginalName();
            $filePath = 'farzam_images/' . $file_name;
            $s3 = Storage::disk('s3');
            $status = $s3->put($filePath, file_get_contents($file), 'public');
            if ($status) {
                $imageurl = $s3->url($filePath);
                $product->image()->insert(
                    [
                        'model_id' => $product->id,
                        'model_type' => 'App\Models\Product',
                        'file_name' => $filePath
                    ]);
                $product['image_url' . $i] = $imageurl;
                $i++;
            }
        }
            return $this->response(true, "Product is Updated", $product);

    }


    public function delete(Delete $request)
    {
        //Get id of product to delete
        $product= Product::find($request['id']);
        //Check whether type exists ot not
        if($product == null) {
            return $this->response(true, "Product is not available", null);
        }
        else{
            //Delete images related to that product
            $ids=Images::select('id')->where('model_type','App\Models\Product')->where('model_id',$product->id)->get();
            S3Controller::deleteProductphotos($ids);
            $product->delete();

        }
            return $this->response(true, "Product is deleted", null);
    }

    public function deletePhotos(Request $request)
    {
        //Delete images of id given in request
        $ids = $request['id'];
        foreach ($ids as $id) {
             if(Images::where('id',$id)->exists()) {
                 $product = Images::find($id);
                 $image_Name = $product['file_name'];
                 $status = Storage::disk('s3')->delete($image_Name);
                 if ($status) {
                     $product->delete();
                 }
             }
        }
        return $this->response(True, "Product Photo Deleted successfully", NULL);
    }

    public function response($status,$message,$data)
    {
        return response()->json(['status'=>$status,'message'=>$message,'data'=>$data]);
    }
}
