<?php

namespace App\Http\Controllers\Inventory;

use App\Http\Controllers\Controller;

use App\Http\Requests\Inventory\Category\Add;
use App\Http\Requests\Inventory\Category\Update;
use App\Http\Requests\Inventory\Category\Delete;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class CategoryController extends Controller
{
    public function index()
    {
        //Will show all categories
        $category=Category::all();
        return $this->response(true,"All Categories",$category);
    }
    public function create(Add $request)
    {
        //Get logged in user
        $token=JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        //Add new category
        $category =Category::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'created_by'=>$user->id
        ]);
        return $this->response(true,"Category is Added",$category);
    }

    public function update(Update $request)
    {
        //Get logged in user
        $token = JWTAuth::parseToken()->getToken();
        $user = JWTAuth::toUser($token);
        //Get id of category to update
        $category_id = Category::find($request['id']);
        //Check whether category exists ot not
        if ($category_id == null) {
            return $this->response(true, "Category is not available", null);
        } else {
            $category = $request->all();
            $request['updated_by'] = $user->id;
            Category::where('id', $category_id->id)->update($request->all());
            return $this->response(true, "Category is Updated", $category);
        }
    }
    public function delete(Delete $request)
    {
        //Get id of category to delete
        $id = Category::find($request['id']);
        //Check whether category exists ot not
        if ($id == null) {
            return $this->response(true, "Category is not available", null);
        } else {
            $category = Category::find($id->id);
            $category->product()->update(['category_id'=>null]);
            //$product= Product::find($category->id);
            //$product->category_id=null;
            $category->delete();
            return $this->response(true, "Category is deleted", null);
        }
    }

    public function response($status,$message,$data){
        return response()->json(['status'=>$status,'message'=>$message,'data'=>$data]);
    }
}

