<?php

use Illuminate\Database\Seeder;
use App\Models\Type;

class TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $data = array(
            array('name'=>'Type-A', 'description'=>$faker->paragraph($nbSentences = 0.5, $variableNbSentences = true),'created_by' => $faker->numberBetween(1,10)),
            array('name'=>'Type-B', 'description'=>$faker->paragraph($nbSentences = 0.5, $variableNbSentences = true),'created_by' => $faker->numberBetween(1,10)),
            array('name'=>'Type-C', 'description'=>$faker->paragraph($nbSentences = 0.5, $variableNbSentences = true),'created_by' => $faker->numberBetween(1,10)),
        );

        Type::insert($data);
    }
}
