<?php

use App\Models\Option;
use Illuminate\Database\Seeder;

class OptionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('color'=>'red', 'size'=>'S'),
            array('color'=>'red', 'size'=>'M'),
            array('color'=>'red', 'size'=>'L'),
            array('color'=>'red', 'size'=>'XL'),

            array('color'=>'blue', 'size'=>'S'),
            array('color'=>'blue', 'size'=>'M'),
            array('color'=>'blue', 'size'=>'L'),
            array('color'=>'blue', 'size'=>'XL'),

            array('color'=>'green', 'size'=>'S'),
            array('color'=>'green', 'size'=>'M'),
            array('color'=>'green', 'size'=>'L'),
            array('color'=>'green', 'size'=>'XL'),

            array('color'=>'black', 'size'=>'S'),
            array('color'=>'black', 'size'=>'M'),
            array('color'=>'black', 'size'=>'L'),
            array('color'=>'black', 'size'=>'XL'),

            array('color'=>'white', 'size'=>'S'),
            array('color'=>'white', 'size'=>'M'),
            array('color'=>'white', 'size'=>'L'),
            array('color'=>'white', 'size'=>'XL'),

            array('color'=>'yellow', 'size'=>'S'),
            array('color'=>'yellow', 'size'=>'M'),
            array('color'=>'yellow', 'size'=>'L'),
            array('color'=>'yellow', 'size'=>'XL'),

            array('color'=>'indigo', 'size'=>'S'),
            array('color'=>'indigo', 'size'=>'M'),
            array('color'=>'indigo', 'size'=>'L'),
            array('color'=>'indigo', 'size'=>'XL'),

        );

        Option::insert($data);
    }
}
