<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ProductView extends Migration
{
   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE
        OR REPLACE View product_view as
        SELECT
        products.name as product,
        products.cost_price,
		products.sale_price,
		categories.name as category,
		types.name as type,
		options.color,
		options.size
		From products

		JOIN categories

		ON products.category_id=categories.id and categories.deleted_at is NULL and products.category_id is not NULL

		JOIN types
		ON products.type_id=types.id and types.deleted_at is NULL and products.type_id is not NULL

		JOIN options
		ON products.option_id=options.id and options.deleted_at is NULL and products.option_id is not NULL

       where products.deleted_at is null;

        ");
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_view');
    }
}
