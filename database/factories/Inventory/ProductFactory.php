<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->bothify('?###??##'),
        'cost_price' => $faker->numberBetween($min = 1500, $max = 6000),
        'sale_price' => $faker->numberBetween($min = 1500, $max = 6000),
        'description' => $faker->paragraph($nbSentences = 0.5, $variableNbSentences = true),
        'category_id' => $faker->numberBetween(1,5),
        'type_id' => $faker->numberBetween(1,5),
        'option_id' => $faker->numberBetween(1,5),
        'created_by' => $faker->numberBetween(1,10)
    ];
});
