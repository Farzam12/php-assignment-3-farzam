<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Models\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->bothify('?###??##'),
        'description' => $faker->paragraph($nbSentences = 0.5, $variableNbSentences = true),
        'created_by' => $faker->numberBetween(1,10)
    ];
});
